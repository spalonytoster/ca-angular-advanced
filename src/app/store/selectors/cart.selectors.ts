import { createFeatureSelector } from "@ngrx/store";

export const cartState = createFeatureSelector('cart');
