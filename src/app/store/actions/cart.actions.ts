import { createAction, props } from '@ngrx/store';

export const AddToCart = createAction(
  '[Cart] Add',
  // props<{ payload: { id: string} }>()
  (id: string) => ({ payload: { id } })
);
