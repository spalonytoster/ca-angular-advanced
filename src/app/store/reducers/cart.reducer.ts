import { AddToCart } from '../actions/cart.actions';

const initialState = { data: [] };

export const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case AddToCart.type:
      return { ...state, data: [...state.data, action.payload] };

    default:
      return state;
  }
};
