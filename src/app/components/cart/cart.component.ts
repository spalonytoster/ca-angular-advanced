import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { cartState } from '../../store/selectors/cart.selectors';
import { AddToCart } from '../../store/actions/cart.actions';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  data$: Observable<any>;

  constructor(private store: Store<any>) {
    store.subscribe(state => {
      console.log('state:', state);

    });
  }

  ngOnInit() {
    this.data$ = this.store.select(cartState);
  }

}
