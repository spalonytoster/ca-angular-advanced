import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CartComponent } from './components/cart/cart.component';

const routes: Routes = [
  { path: 'cart', component: CartComponent, loadChildren: './modules/list/list.module#ListModule' },
  // { path: 'register-form', loadChildren: './modules/shared/shared.module#SharedModule' }

  { path: 'register', loadChildren: './modules/register/register.module#RegisterModule' },
  { path: 'list', loadChildren: './modules/list/list.module#ListModule' },
  { path: 'game', loadChildren: './modules/game/game.module#GameModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
