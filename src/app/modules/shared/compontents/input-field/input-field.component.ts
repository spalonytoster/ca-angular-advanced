import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormControl } from '@angular/forms';
import { config } from 'rxjs';

@Component({
  selector: 'app-input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.scss']
})
export class InputFieldComponent implements OnInit {

  form: FormGroup;
  config: any;
  formControl: FormControl;

  required: boolean = false;
  maxLength: number = 0;

  constructor() {

  }

  ngOnInit() {
    this.formControl = new FormControl(this.config.name);
    this.setupValidations();
  }

  setupValidations() {
    const validations: any[] = this.config.validation;

    if (validations) {
      this.required = validations.includes(el => el === 'required');
      // this.maxLength = validation.includes
    }
  }
}
