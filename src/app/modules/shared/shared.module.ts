import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormGeneratorComponent } from './form-generator/form-generator.component';
import { FieldGeneratorDirective } from './directives/field-generator.directive';
import { InputFieldComponent } from './compontents/input-field/input-field.component';
import { ButtonComponent } from './compontents/button/button.component';
import { SelectComponent } from './compontents/select/select.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule, MatOptionModule, MatSelectModule, MatButtonModule, MatCheckboxModule } from '@angular/material';
import { CheckboxComponent } from './compontents/checkbox/checkbox.component';

@NgModule({
  declarations: [FormGeneratorComponent, FieldGeneratorDirective, InputFieldComponent, ButtonComponent, SelectComponent, CheckboxComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatOptionModule,
    MatButtonModule,
    MatCheckboxModule
  ],
  entryComponents: [
    InputFieldComponent,
    SelectComponent,
    ButtonComponent,
    CheckboxComponent
  ],
  exports: [FormGeneratorComponent]
})
export class SharedModule { }
