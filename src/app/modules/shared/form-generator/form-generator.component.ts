import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, Form, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-generator',
  templateUrl: './form-generator.component.html',
  styleUrls: ['./form-generator.component.scss']
})
export class FormGeneratorComponent implements OnInit, OnChanges {

  @Input() config: any;
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = new FormGroup({});
  }

  ngOnInit() {

  }

  ngOnChanges({ config: { currentValue } }: SimpleChanges) {
    if (currentValue) {
      this.createForm(currentValue);
    }
  }

  createForm(config: any) {
    config
      .filter(conf => conf.type !== 'button')
      .forEach(conf => {
        console.log('form control:', conf);
        this.form.addControl(conf.name, this.fb.control(conf.value, this.makeValidators(conf.validation)));
      });
  }

  makeValidators(validation: any) {
    if (!validation) return;
    return validation.map((validator) => {
      if (/object/.test(typeof validator)) {
        const [fn, param] = validator;
        if (fn in Validators) {
          return Validators[fn](param);
        }
      } else {
        if (validator in Validators) {
          return Validators[validator];
        }
      }
    })
  }

  onSubmit($event) {
    console.log('form has been submitted!');
    console.log(this.form.value);
    console.log($event);

  }

}
