import { Directive, Input, OnInit, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { InputFieldComponent } from '../compontents/input-field/input-field.component';
import { SelectComponent } from '../compontents/select/select.component';
import { ButtonComponent } from '../compontents/button/button.component';
import { FormGroup } from '@angular/forms';
import { CheckboxComponent } from '../compontents/checkbox/checkbox.component';

const fields = {
  input: InputFieldComponent,
  select: SelectComponent,
  button: ButtonComponent,
  checkbox: CheckboxComponent
};

@Directive({
  selector: '[appFieldGenerator]'
})
export class FieldGeneratorDirective implements OnInit {

  @Input() appFieldGenerator: any[];

  form: FormGroup;
  fieldConfig: any;

  constructor(
    private container: ViewContainerRef,
    private resolver: ComponentFactoryResolver) { }

  ngOnInit(): void {
    this.fieldConfig = this.appFieldGenerator[0];
    this.form = this.appFieldGenerator[1];
    this.generateField();
  }

  generateField() {
    const componentFactory = this.resolver.resolveComponentFactory(fields[this.fieldConfig.type]);
    const component = this.container.createComponent(componentFactory);
    const componentInstance: any = component.instance;
    componentInstance.config = this.fieldConfig;
    componentInstance.form = this.form;
  }
}
