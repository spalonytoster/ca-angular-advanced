import { AfterViewInit, Component, ViewChild, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DatagridDataSource } from './datagrid-datasource';

@Component({
  selector: 'app-datagrid',
  templateUrl: './datagrid.component.html',
  styleUrls: ['./datagrid.component.css']
})
export class DatagridComponent implements OnInit, AfterViewInit, OnChanges {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: DatagridDataSource;

  @Output() action = new EventEmitter();

  @Input() set data(value: any[]) {
    value && this.init(value);
  }

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'title', 'img'];

  ngOnChanges() {

  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    // this.dataSource = new DatagridDataSource(this.paginator, this.sort);
  }

  init(value) {
    this.dataSource = new DatagridDataSource(this.paginator, this.sort);
    this.dataSource.data = value;
  }

  buy(id) {
    this.action.emit(id);
  }
}
