import { Component, OnInit } from '@angular/core';
import { DatagridService } from '../datagrid.service';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AddToCart } from '../../../store/actions/cart.actions';
import { cartState } from '../../../store/selectors/cart.selectors';

@Component({
  selector: 'app-list-form',
  templateUrl: './list-form.component.html',
  styleUrls: ['./list-form.component.scss']
})
export class ListFormComponent implements OnInit {

  data$: Observable<any>;

  constructor(
    private datagridService: DatagridService,
    private store: Store<any>) { }

  ngOnInit() {
    this.data$ = this.datagridService.fetch();
    this.data$.subscribe(data => console.log(data));
  }

  buy(id: string) {
    console.log(id);
    this.store.dispatch(AddToCart(id));

    this.store.select(cartState)
      .subscribe(item => console.log('item:', item));
  }
}
