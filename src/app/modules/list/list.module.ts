import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListFormComponent } from './list-form/list-form.component';
import { RouterModule } from '@angular/router';
import { DatagridComponent } from './datagrid/datagrid.component';
import { MatTableModule, MatPaginatorModule, MatSortModule, MatButtonModule } from '@angular/material';

@NgModule({
  declarations: [ListFormComponent, DatagridComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: ListFormComponent }
    ]),
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule
  ],
  exports: [ListFormComponent]
})
export class ListModule { }
