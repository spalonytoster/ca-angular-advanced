import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Api } from '../../utils/api';

@Injectable({providedIn: 'root'})
export class DatagridService {
  constructor(private httpClient: HttpClient) { }

  fetch() {
    return this.httpClient.get(Api.ITEMS_END_POINT)
      .pipe(
        map(({ data }: any) => data)
      );
  }

}
