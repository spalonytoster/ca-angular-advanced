import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterFormComponent } from './register-form/register-form.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [RegisterFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: RegisterFormComponent }
    ]),
    SharedModule
  ],
  exports: [RegisterFormComponent]
})
export class RegisterModule { }
