import { Component, OnInit } from '@angular/core';
import { RegisterService } from '../register.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {

  constructor(private registerService: RegisterService) { }

  config: any;

  ngOnInit() {
    this.registerService.fetchConfig()
      .pipe(
        map(({ data }) => {
          const checkboxConfig = {
            type: "checkbox",
            label: "i consent to the rules",
            name: "agreement",
            value: true,
            validation: ["required"]
          };

          return { data: [...data, checkboxConfig] };
        })
      )
      .subscribe(({ data }: any) => {
        this.config = data;
        console.log(this.config);

      });
  }
}
