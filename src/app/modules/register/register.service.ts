import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
// const formConfig = require('./form-config.json');

@Injectable({providedIn: 'root'})
export class RegisterService {
  constructor(private http: HttpClient) { }

  fetchConfig(): Observable<any> {
    return this.http.get('https://api.emitter.pl/form-config');
    // return new BehaviorSubject<any>(formConfig);
  }
}
