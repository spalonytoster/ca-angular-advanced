import { Component, OnInit, ViewChild, TemplateRef, ViewContainerRef } from '@angular/core';
import { GameService } from '../services/game.service';
import { fromEvent } from 'rxjs';
import { throttleTime } from 'rxjs/operators';

@Component({
  selector: 'app-game-form',
  templateUrl: './game-form.component.html',
  styleUrls: ['./game-form.component.scss']
})
export class GameFormComponent implements OnInit {

  ws: WebSocket;
  @ViewChild('avatar') avatar: TemplateRef<any>;
  map: Map<any, any> = new Map();

  constructor(
    private gameService: GameService,
    private container: ViewContainerRef
  ) { }

  ngOnInit() {
    console.log('game onInit 9');

    this.gameService.getUser()
      .subscribe(({ username }): any => {
        username
          ? this.init()
          : this.register();
      });
  }

  init() {
    this.ws = this.gameService.connect();

    this.ws.onopen = (res) => {
      fromEvent(document.body, 'mousemove')
        .pipe(
          throttleTime(50)
        )
        .subscribe(({ clientX, clientY }: MouseEvent) => {
          this.ws.send(JSON.stringify({ clientX, clientY }));
        });
    };

    this.ws.onmessage = (({ data }) => {
      this.updateAvatar(JSON.parse(data));
    });
  }

  updateAvatar(data: any) {
    const user = this.map.get(data.username);

    if (user) {
      user.context.$implicit = data;
    } else {
      const view = this.container.createEmbeddedView(this.avatar, { $implicit: data });
      this.map.set(data.username, view);
    }
  }

  register() {
    const username = prompt('your name');
    if (/^[a-z]+$/.test(username)) {
      this.gameService.registerUser(username).subscribe((resp) => {
        debugger;
      });
    } else {
      alert('only letters without spaces please');
    }
  }
}
