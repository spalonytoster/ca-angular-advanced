import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private ws: WebSocket;

  constructor(private http: HttpClient) { }

  connect(): any {
    this.ws = new WebSocket('wss://game.emitter.pl');
    return this.ws;
  }

  registerUser(username: string): any {
    this.http.post('//game.emitter.pl/register', { username }, { withCredentials: true });
  }

  getUser(): Observable<any> {
    return this.http.get('http://game.emitter.pl/get-user', { withCredentials: true });
  }

}
